# encoding: utf-8
"""
The day_or_night plugin
-----------------------

Analyses the time and position of a miniprod, returns the following
values:

night
  the miniprod is night if the lowest solar zenith angle is > 110 degrees.
  i.e. no part of the miniprod is in twilight or day

twilight
  twilight is returned if 90 degrees < lowest solar zenith angle < 110 degrees.
  i.e. any par of the miniprod is in twilight, but no part is in night.

day
  day is returned only if all parts of the miniprod have a solar zenith angle
  less than 90 degrees

It operates on an entire miniprod, and not just the numerical values of a
particular field.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import datetime

from dateutil import parser
from numpy import pi, arcsin, sin, cos, arccos

from felyx_processor.basetypes import MINIPRODUCT
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import BaseStatisticPlugin


info = PluginMetadata(
    'day_or_night', 'DayOrNight',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class DayOrNight(BaseStatisticPlugin):
    """
    A plugin to tell if a miniprod is exclusively in either day or night. The
    result will be 'twilight' if the miniprod is not exclusively in day or
    night.
    """
    @classmethod
    def get_sun_zenital_angle(cls, time, lat, lon):
        '''
        compute sun zenital angle from time and location

        Args:
            time (datetime or tuple): time
            lat (float): latitude, in degrees
            lon (float): longitude, in degrees

        Returns:
            the calculated sun zenital angle, in degrees
        '''
        if isinstance(time, datetime.datetime):
            day = time.timetuple()[7]
            hour = time.hour
            minute = time.minute
        elif isinstance(time, tuple):
            day, hour, minute = time

        w_angle = 2 * pi / 365.25
        jquan = day
        sin1 = sin(w_angle * (jquan - 2.))
        decl = arcsin(0.398 * sin(w_angle * (jquan - 81. + 2. * sin1)))
        eqtm = 0.128 * sin1 + 0.164 * sin(2. * w_angle * (jquan + 10.))
        htsv = hour + minute / 60. - eqtm + lon / 15.
        anghor = pi * (htsv - 12.) / 12.
        lat_rad = lat * pi/180.
        sla = sin(lat_rad)
        cla = cos(lat_rad)
        sde = sin(decl)
        cde = cos(decl)
        cah = cos(anghor)
        zensol = arccos(sla * sde + cla * cde * cah)
        return zensol * 180 / pi

    @classmethod
    def description(cls, angle):
        """
        Helper function for descriptions

        :param float angle: The angle solar zenith angle in degrees.
        :return: Day, night or twilight
        :rtype: str
        """
        if angle > 110:
            return 'night'
        elif angle > 90:
            return 'twilight'
        else:
            return 'day'

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'day_or_night'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['day_or_night']

    @staticmethod
    @operator(
        'metadata', 1,
        {
            'type': list,
            'userDefined': False,
            'doc': 'A list of Data objects for analysis'
        }
    )
    def day_or_night(data_list):
        """
        Return 'day' if the miniprod is exclusively in daylight, return 'night'
        if the miniprod is exclusively in night time and return 'twilight'
        under any other circumstance.
        ---
        :param list data_list: A list of Data objects for analysis.
        :return: A numerical value for each element in data_list.
        :rtype: list
        """

        results = []
        if not all([_.datatype == MINIPRODUCT for _ in data_list]):
            raise NotImplementedError(
                'This plugin only supports miniprods as input'
            )

        for miniprod in data_list:

            # Establish the solar zenith angles for the extremities of
            # the miniprod.
            angles = []
            lats = [
                miniprod.content.read_global_attribute(
                    'geospatial_lat_min'
                ),
                miniprod.content.read_global_attribute(
                    'geospatial_lat_max'
                )
            ]
            lons = [
                miniprod.content.read_global_attribute(
                    'geospatial_lon_min'
                ),
                miniprod.content.read_global_attribute(
                    'geospatial_lon_max'
                )
            ]
            times = [
                parser.parse(
                    miniprod.content.read_global_attribute(time)
                ) for time in [
                    'time_coverage_start', 'time_coverage_end'
                ]
            ]

            for time in times:
                for lat in lats:
                    for lon in lons:
                        angles.append(
                            DayOrNight.description(
                                DayOrNight.get_sun_zenital_angle(
                                    time, lat, lon
                                )
                            )
                        )

            result = list(set(angles))

            # Ensure the result is 'twilight' under any circumstance other
            # than exclusive daytime coverage or exclusive nighttime
            # coverage
            if 'twilight' in result:
                result = 'twilight'
            elif len(result) == 2:
                result = 'twilight'
            else:
                result = result[0]

            results.append(result)

        return results
