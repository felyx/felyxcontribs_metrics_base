# encoding: utf-8
"""
The coefficient_of_correlation plugin
-------------------------------------

This module is a felyx plugin, designed to calculate the coefficient of
 correlation between the two datasets passed to it.

It utilises the BivariateStatisticPlugin as it requires two arrays or
two lists of equal length miniprod references, and returns only a single value
per input field.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

from numpy import ndarray
from numpy.ma.core import MaskedArray
from scipy.stats import pearsonr
from scipy.stats.mstats import pearsonr as mpearsonr

from felyx_processor.basetypes import MINIPRODUCT, METRICS
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    BivariateStatisticPlugin

info = PluginMetadata(
    'coefficient_of_correlation', 'CoefficientOfCorrelation',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class CoefficientOfCorrelation(BivariateStatisticPlugin):
    """
    A plugin to return the coefficient of correlation between
    two inputs.
    """
    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'coefficient_of_correlation'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['coefficient_of_correlation']

    @staticmethod
    @operator(
        'statistics', 1,
        {'type': list, 'userDefined': False, 'doc': ''},
        {'type': list, 'userDefined': False, 'doc': ''},
        {'type': str, 'userDefined': True, 'doc': ''},
        {'type': bool, 'userDefined': True, 'doc': ''},
        {'type': list, 'userDefined': True, 'doc': ''}
    )
    def coefficient_of_correlation(
            x_data, y_data, x_field='', y_field='', limit_to_site=True,
            must_have=None):
        """
        Returns the coefficient of correlation of the input values. These
        may be singular arrays, in which case the field keywords are ignored,
        or they may be collections of miniprods, in which case the field
        keyword is used.
        ---
        The values for limit_to_site and must_have are applied to
        each miniprod identically, or ignored in the case of non-miniprod
        inputs.

        The axis keyword is ignored in all cases.
        :param x_data: A list of Data objects for analysis, each element
            constituting the first term of the correlation.
        :type x_data: list or numpy.ndarray
        :param y_data: A list of Data objects for analysis, each element
            constituting the second term of the correlation.
        :type x_data: list or numpy.ndarray
        :param str x_field: The name of the field in x_data miniprods to read.
        :param str y_field: The name of the field in x_data miniprods to read.
        :param bool limit_to_site: Limit analysis to pixels strictly within the
            felyx site, False to analyse all pixels in the miniprod.
        :param list must_have: A list of restriction dictionary elements.
        :return: A numerical value, or array for each element in data_list.
        :rtype: list
        """
        result = []
        if isinstance(x_data, MaskedArray) or isinstance(y_data, MaskedArray):
            # The inputs are masked arrays
            result.append(mpearsonr(x_data.flatten(), y_data.flatten())[0])

        elif isinstance(x_data, ndarray) or isinstance(y_data, ndarray):
            # The inputs are ndarrays
            # noinspection PyUnresolvedReferences
            result.append(pearsonr(x_data.flatten(), y_data.flatten())[0])

        elif all([_.datatype == MINIPRODUCT for _ in x_data]) and \
                all([_.datatype == MINIPRODUCT for _ in y_data]):
            # The inputs are data_lists.
            for miniprod_x, miniprod_y in x_data, y_data:

                # Read the x values
                x_value = miniprod_x.content.read_values(
                    x_field, limit_to_site=limit_to_site,
                    must_have=must_have
                ).flatten()

                # Read the y_values
                y_value = miniprod_y.content.read_values(
                    y_field, limit_to_site=limit_to_site,
                    must_have=must_have
                ).flatten()

                result.append(mpearsonr(x_value, y_value)[0])

        elif any([_.datatype == METRICS for _ in x_data]) or \
                any([_.datatype == METRICS for _ in y_data]):
            # Some inputs are metrics

            raise NotImplementedError(
                'This plugin is not yet defined for metrics'
            )

        return result
