# encoding: utf-8
"""
The centre value plugin
-----------------------

This module is a felyx plugin, designed to return the centre value of a
field. It utilises the UnivariateStatisticPlugin as it accepts only a single
field for analysis, and returns only a single value per input field.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
from felyx_processor.basetypes import MINIPRODUCT
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    UnivariateStatisticPlugin


info = PluginMetadata(
    'centre_value', 'CentreValue',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class CentreValue(UnivariateStatisticPlugin):
    """
    A plugin to return the centre value of a field in a series of list of
    felyx objects.
    """

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'centre_value'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['centre_value']

    # noinspection PyUnusedLocal
    @staticmethod
    @operator(
        'statistics', 1,
        {'type': list, 'userDefined': False,
         'doc': 'A list of Data objects'},
        {'type': str, 'userDefined': True,
         'doc': 'The name of the field to analyse'},
        {'type': bool, 'userDefined': True,
         'doc': 'Limit analysis to pixels strictly within the felyx site, '
                'False to analyse all pixels in the miniprod'},
        {'type': list, 'userDefined': False,
         'doc': 'Other arguments are ignored for this plugin'}
    )
    def centre_value(data_list, field='', limit_to_site=True, **kwargs):
        """Returns the value of the pixel closest to the centre of the
        miniprod (with limit_to_site=False) or the centre of the felyx
        site (default).
        ---
        :param list data_list: A list of Data objects.
        :param bool limit_to_site: Limit analysis to pixels strictly within
          the felyx site, False to analyse all pixels in the miniprod.
        :param kwargs: Other arguments are ignored for this plugin.
        :param str field: The name of the field to analyse.
        :return: A numerical value for each element in data_list.
        :rtype: list
        """

        if not all([_.datatype == MINIPRODUCT for _ in data_list]):
            raise NotImplementedError(
                'This plugin only supports miniprods as input'
            )

        results = []
        for miniprod in data_list:
            # For each miniprod, read the requested values
            value = miniprod.content.read_values(
                field, limit_to_site=limit_to_site, must_have=None,
            ).flatten()

            # Get the centre value
            result = value[value.size / 2]

            results.append(result)

        return results
