# -*- coding: utf-8 -*-
from __future__ import with_statement

import traceback

import numpy
import scipy.ndimage

from cerbere.dataset.field import Field
from cerbere.dataset.ncdataset import NCDataset
from felyx_processor.processors.plugins import PluginMetadata, operator

info = PluginMetadata('gradient', 'Gradient',
                      ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class VariableNotFoundException(Exception):
    """ """
    pass


class Gradient(object):
    """ Plugin that computes gradients """

    # Tools
    # -------------------------------------------------------------------------
    @staticmethod
    def _compute_sobel_gradient(data):
        """ """
        print('*** Compute Sobel ***')
        print(str(data))
        dx = scipy.ndimage.sobel(data, 0)
        dy = scipy.ndimage.sobel(data, 1)
        magnitude = numpy.hypot(dx, dy)
        normalized = magnitude / numpy.max(magnitude)
        return normalized

    # Operators
    # -------------------------------------------------------------------------
    @staticmethod
    @operator(
        'processing', 5,
        {'type': NCDataset, 'userDefined': False,
         'doc': 'Cerbere model that contains the data to process'},
        {'type': str, 'userDefined': True,
         'doc': 'Name of the input variable'},
        {'type': str, 'userDefined': True,
         'doc': 'Name of the ouput variable'}
    )
    def sobel_gradient(input, var_src, var_dst):
        """ Operator that computes a Sobel gradient.
            ---
            @param input: Cerbere model that contains the data to process.
            @param var_src: Name of the input variable.
            @param var_dst: Name of the ouput variable.
        """
        if not input.content.has_field(var_src):
            raise VariableNotFoundException('Variable "%s" not found' % var_src)

        src_field = input.content.extract_field(var_src, None)
        print('Dimensions: ' + str(src_field.dimensions))
        for dimname in src_field.dimensions:
            print('\t%s: %i' % (src_field.dimensions[dimname],
                                src_field.get_dimsize(dimname)))

        try:
            if 'time' in src_field.dimensions:
                # Expected dimensions: time, lat, lon
                result = numpy.ndarray(map(lambda x: src_field.get_dimsize(x),
                                           src_field.dimensions.keys()))
                for i in range(0, src_field.get_dimsize('time')):
                    data_at_t = src_field.get_values(
                        slices={'time': slice(i, i+1, 1)})[0]
                    result[i] = Gradient._compute_sobel_gradient(data_at_t)
            else:
                # Consider data as a 2D array
                result = Gradient._compute_sobel_gradient(
                    src_field.get_values())
        except Exception:
            print(traceback.print_exc())

        # Create the new variable
        f = Field(
            result,
            name=var_dst,
            dims=src_field.dimensions)
        input.content.add_field(f)

        print('Sobel done')
        print(input.content.get_fieldnames())

        # Changes are made in place so return input as output
        return input

    # Plugin interface implementation
    # -------------------------------------------------------------------------
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['sobel_gradient']

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def perform(task_id, task_name, *args, **kwargs):
        """ """
        input_file = args[0]

        if 'sobel_gradient' == task_id:
            output = Gradient.sobel_gradient(input=input_file,
                                             var_src=kwargs['var_src'],
                                             var_dst=kwargs['var_dst']
                                             )
        else:
            return False, None, '...'
        return True, output, 'Success'
