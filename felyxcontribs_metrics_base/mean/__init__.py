# encoding: utf-8
"""
The mean
---------------

This module is a felyx mean, designed to calculate
   the mean value of data passed to it.

It utilises the UnivariateStatisticPlugin as it accepts only a single
   field for analysis, and returns only a single value per input field.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
from felyx_processor.basetypes import MINIPRODUCT
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    UnivariateStatisticPlugin


info = PluginMetadata(
    'mean', 'Mean',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class Mean(UnivariateStatisticPlugin):
    """
    A mean to return the mean value of a series of felyx inputs.
    """

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is mean'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['mean']

    @staticmethod
    @operator(
        'statistics', 1,
        {'type': list, 'userDefined': False,
         'doc': 'A list of Data objects for analysis'},
        {'type': str, 'userDefined': True,
         'doc': 'The name of the field to analyse'},
        {'type': int, 'userDefined': True,
         'doc': 'Optional dimension value for which to return a vector '
                'or results'},
        {'type': bool, 'userDefined': True,
         'doc': 'Limit analysis to pixels strictly within the felyx site, '
                'False to analyse all pixels in the miniprod'},
        {'type': list, 'userDefined': True,
         'doc': 'A list of restriction dictionary elements'}
    )
    def mean(
            data_list, field='', axis=None, limit_to_site=True,
            must_have=None):
        """
        Returns the mean of the values in a field, for each in data_list.
        Additionally, accepts an 'axis' keyword, which will return an
        array of means for each field along that axis. Leaving this
        blank will return a single value form the entire array of input.
        ---
        :param bool limit_to_site: Limit analysis to pixels strictly within the
          felyx site, False to analyse all pixels in the miniprod.
        :param list data_list: A list of Data objects for analysis.
        :param str field: The name of the field to analyse.
        :param int axis: Optional dimension value for which to return a vector
          or results.
        :param list must_have: A list of restriction dictionary elements.
        :return: A numerical value, or array for each element in data_list.
        :rtype: list

        """

        if not all([_.datatype == MINIPRODUCT for _ in data_list]):
            raise NotImplementedError(
                'This mean only supports miniprods as input'
            )

        results = []
        for miniprod in data_list:
            value = miniprod.content.read_values(
                field, limit_to_site=limit_to_site,
                must_have=must_have
            ).mean(axis=axis)

            results.append(value)

        return results
